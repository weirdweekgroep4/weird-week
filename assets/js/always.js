var questions = [
		{
			"question":"What is Cypher’s real last name?",
			"options":["Johnson", "Reagan", "Nixon", "Clinton"],
			"answer":1,
			"time":0
		},
		{
			"question":"What is the phrase written above the door in the Oracle’s kitchen?",
			"options":["Caveat Emptor", "Veni Vidi Vici", "Temet Nosce", "Et Tu Brute"],
			"answer":2,
			"time":0
		},
		{
			"question":"What is Neo’s real name?",
			"options":["John P. Black","Timothy J. Washington","James L. Burns","Thomas A. Anderson"],
			"answer":3,
			"time":0
		},
		{
			"question":"By what name are the sentinels more commonly referred to?",
			"options":["Squiddies","Bogeys","Reapers","Sweepers"],
			"answer":0,
			"time":0
		},
		{
			"question":"The predominant wardrobe color in the Matrix is black. Which character defies this trend?",
			"options":["Trinity","Mouse","Switch","Apoc"],
			"answer":2,
			"time":0
		},
		{
			"question":"What is the name of Morpheus’s ship?",
			"options":["Voyager","Nebuchadnezzar","Hammer","Misanthrope"],
			"answer":1,
			"time":0
		},
		{
			"question":"From what telephone number does Trinity contact Cypher during the opening credits?",
			"options":["555-0690","555-1234","555-0001","555-5309"],
			"answer":0,
			"time":0
		},
		{
			"question":"When taken, which of the pills Morpheus offers allows the recipient to remain inside the Matrix?",
			"options":["Red","Blue","Yellow","Purple"],
			"answer":1,
			"time":0
		},
		{
			"question":"What television show personality’s name does Neo call Tank over the cell phone?",
			"options":["Bill Nye","Beakman","Superman","Mr. Wizard"],
			"answer":3,
			"time":0
		},
		{
			"question":"Three agents are featured in the first Matrix movie. We know Agent Smith. What are the name of the other two?",
			"options":["Brown and Jones","Johnson and Jackson","Black and Davis","Thompson and Davidson"],
			"answer":0,
			"time":0
		},
		{
			"question":"Neo surprises Cypher while he’s “monitoring the Matrix”. What is he supposedly watching/looking at?",
			"options":["Baseball","Agents","Girls","Police Activity"],
			"answer":2,
			
			"time":0
		},
		{
			"question":"What animals are shown on the TV in the Oracle’s apartment?",
			"options":["Dogs","Birds","Tigers","Rabbits"],
			"answer":3,
			"time":0
		},
		{
			"question":"What is the first martial art form Neo “learns”?",
			"options":["Ju Juitsu/Ju-Jitsu","Karate","Tae Kwon Do","Kung Fu"],
			"answer":0,
			"time":0
		},
		{
			"question":"What profession does Cypher wish to have when he re-enters the Matrix?",
			"options":["Politician","Agent","Baseball Player","Actor"],
			"answer":3,
			"time":0
		},
		{
			"question":"What popular children’s book is referenced by Morpheus when trying to explain the Matrix to Neo?",
			"options":["The Wizard of Oz","Alice in Wonderland","James and the Giant Peach","The Cat in the Hat"],
			"answer":1,
			"time":0
		}
	],
	questionIndex = 0,
	timeRequired  = 13.00,
	time          = 8.00,
	end           = .01,
	interval      = setInterval(() => {
		if ( time <= end || timeRequired <= end )
		{
			if ( interval && interval.clearInterval )
				interval.clearInterval();

			return;
		}

		time         -= 0.01;
		timeRequired -= 0.01;

		var gameover = time         <= end,
			winner   = timeRequired <= end;

		if ( gameover || winner )
		{
			if ( gameover )
			{
				hideGame();
				$('#gameover').get()[0].style.display = 'block';
			}
			else
			{
				$('#simulation #phonebooth path').each((index, element) => {
					new Segment(element).draw("100%", "100%", 1);
				});

				setTimeout(() => {
					hideGame();
					$('#winner').get()[0].style.display = 'block';
				}, 1500);
			}

			setTimeout(() => {
				window.location.reload();
			}, 5000);
		}

		$('#simulation #time')
			.html(time.toFixed(2) + ' Seconds Before Impact');

		$('#simulation #timeRequired')
			.html(timeRequired.toFixed(2) + ' Seconds Before Escape');

		$('#truck')
			.get()[0]
			.style
			.left = ((-time*10)+80) + '%';

	}, 10),
	showQuestion = () => {
		var object = questions[questionIndex++];

		$('#question')
			.html(object.question);

		$('#optionA')
			.html(object.options[0]);

		$('#optionB')
			.html(object.options[1]);

		$('#optionC')
			.html(object.options[2]);

		$('#optionD')
			.html(object.options[3]);

		console.log(object.answer);
	},
	handleAnswer = _.curry((chosenIndex, event) => {
		var timeNeeded = (timeRequired-time).toFixed(1);
		
		if ( chosenIndex === questions[questionIndex-1].answer )
		{
			if ( time >= 7.5 )
				time = 8;
			else
				time += .5;

			$('#score')
				.removeClass('redText')
				.addClass('greenText')
				.html('Correct! Time Reversed By 1 Second! '+timeNeeded+' Seconds Still Needed!')
				.css({ display: 'block' });
		}
		else
			$('#score')
				.removeClass('greenText')
				.addClass('redText')
				.html('Wrong! '+timeNeeded+' Seconds Still Needed!')
				.css({ display: 'block' });

		setTimeout(() => {
			$('#score').css({ display: 'none' });
		}, 500);

		showQuestion();
	}),
	hideGame = () => {
		$('#title, #simulation, #quiz').each((index, element) => {
			element.style.display = 'none';
		});
	};

showQuestion();

$('#quiz #optionA').click(handleAnswer(0));
$('#quiz #optionB').click(handleAnswer(1));
$('#quiz #optionC').click(handleAnswer(2));
$('#quiz #optionD').click(handleAnswer(3));

$('#autoplay').click(event => {
	var timers = [0, .8, 1.6, 2.4, 3.4, 4.3, 5.2, 6.2, 7.0, 7.8, 8.9, 10.0, 11.1, 12.0],
		index  = 0,
		errors = _.sampleSize(_.times(timers.length), 2);

	_.forEach(timers, (time) => {
		setTimeout(() => {
			handleAnswer(errors.includes(++index) ? 5 : questions[questionIndex-1].answer, undefined);
		}, time*1000);
	});
});